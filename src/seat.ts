import { Section } from "./section";

export interface Seat {
    row: number;
    column: number;
    price: number;
    reserved: boolean;
    section: Section;
}
