import { Seat } from "./seat";

export interface SeatBlock {
    seats: Seat[],
    totalPrice: number;
}