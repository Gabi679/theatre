import { SectionType } from "./sectionType";
import { Section } from "./section";
import { Seat } from "./seat";
import { SeatBlock } from "./seatBlock";

const args = process.argv

//check if the required params are given
if(isNaN(parseInt(args[2]))){
    throw Error('The amount of reserved seats it not given or it is not a number!')
}

if(isNaN(parseInt(args[3]))){
    throw Error('The amount of wanted seats it not given or it is not a number!')
}

let reservedSeatsNumber = parseInt(args[2]);
const seatsNextToEachOther = parseInt(args[3]);

//create the theatre map
const theaterMap = createMap();

//store the map in an array
const mapInArray: Section[] = [];
Object.values(theaterMap).forEach(item=>mapInArray.push(item));

//count the seats
const totalNumberOfSeats = mapInArray.map(section=>section.seats.length).reduce((partialSum, seats) => partialSum + seats, 0);

//check if the reservedSeatsNumber param is acceptable
if(totalNumberOfSeats*0.2 < reservedSeatsNumber){
    throw Error('Too many reserved seats!')
}

//reserve random seats
while(reservedSeatsNumber){
    const randomSection = mapInArray[Math.floor((Math.random() * 8))];
    const randomSeatNumber = Math.floor((Math.random() * randomSection.seats.length))
    if(!randomSection.seats[randomSeatNumber].reserved){
        randomSection.seats[randomSeatNumber].reserved = true;
        reservedSeatsNumber--;
    }
}

//find all available seats
const freeSeatsBlocks: SeatBlock[] = findFreeSeatsBlocks(seatsNextToEachOther);

//find the expensive seat blocks 
const expensiveSeatsInSeatBlocks = freeSeatsBlocks.map(findMostExpensiveSeatsInEachSeatBlock);

//find the most expensive seat blocks 
const mostExpensiveSeatBlocks = expensiveSeatsInSeatBlocks.sort((seatBlock1: SeatBlock, seatBlock2: SeatBlock)=>{
    return seatBlock2.totalPrice - seatBlock1.totalPrice;
}).filter((seatBlock, index, originalSeatBlock)=>seatBlock.totalPrice>=originalSeatBlock[0].totalPrice);

//find the seat blocks in the best section
const seatsWithBestSection = mostExpensiveSeatBlocks.sort((seatBlock1: SeatBlock, seatBlock2: SeatBlock)=>{
    return seatBlock1.seats[0].section.priority - seatBlock2.seats[0].section.priority;
}).filter((seatBlock, index, originalSeatBlock)=>seatBlock.seats[0].section.priority<=originalSeatBlock[0].seats[0].section.priority);

//find the closest seat blocks to the stage.
const seatCloserToTheStage = seatsWithBestSection.sort((seatBlock1: SeatBlock, seatBlock2: SeatBlock)=>{
    return seatBlock1.seats[0].row - seatBlock2.seats[0].row;
}).filter((seatBlock, index, originalSeatBlock)=>seatBlock.seats[0].row<=originalSeatBlock[0].seats[0].row)[0];

if(!seatCloserToTheStage){
    console.log("Sorry the is not enough place for you.");
}else{
    const indexOfS = Object.values(SectionType).indexOf(seatCloserToTheStage.seats[0].section.type as unknown as SectionType);
    const areaName = Object.keys(SectionType)[indexOfS];
    console.log(`The selected seat for you is the ${areaName.replaceAll('_', ' ').toLocaleLowerCase()} area, at the ${seatCloserToTheStage.seats[0].row} row, seat numbers: ${seatCloserToTheStage.seats.map(seat=>seat.column).join(', ')}`)
}


function findMostExpensiveSeatsInEachSeatBlock(seatBlock: SeatBlock): SeatBlock{
        let maxSum = 0;
        let firstIndexOfTheBestSeats = 0;
        //sum the first X seats
        for (let i = 0; i < seatsNextToEachOther; i++) {
            maxSum += seatBlock.seats[i].price;
        }
        for (let i = seatsNextToEachOther; i < seatBlock.seats.length; i++) {
            const lastAndFirstItemDifference = -seatBlock.seats[i - seatsNextToEachOther].price + seatBlock.seats[i].price;
            if (maxSum + lastAndFirstItemDifference > maxSum) {
                maxSum += lastAndFirstItemDifference;
                firstIndexOfTheBestSeats = i-seatsNextToEachOther;
            }else if(maxSum + lastAndFirstItemDifference === maxSum){
                //if the price is the same, then try to check if the position is better
                // check the row length, select the middle of it, then tke the avarage of the first and last selected seat, if the new selection is closer to the middle then it's better.
                const rowlength = seatBlock.seats[i].section.seats.filter(seat=>seat.row === seatBlock.seats[i].row).length;
                const middleSeatNumber = rowlength/2;
                const newAvarageSeatNumber = (seatBlock.seats[i - seatsNextToEachOther+1].column + seatBlock.seats[i].column)/2;
                const oldAvarageSeatNumber = (seatBlock.seats[i - seatsNextToEachOther].column + seatBlock.seats[i-1].column)/2;
                if(Math.abs(middleSeatNumber-newAvarageSeatNumber)<Math.abs(middleSeatNumber-oldAvarageSeatNumber)){
                    firstIndexOfTheBestSeats = i-seatsNextToEachOther;
                }
            }
        }
        seatBlock.seats = seatBlock.seats.filter((seatBlock, index)=>{return index >= firstIndexOfTheBestSeats && index<firstIndexOfTheBestSeats + seatsNextToEachOther});
        seatBlock.totalPrice = maxSum;
        return seatBlock;
}

function findFreeSeatsBlocks(seatsNextToEachOther: number):SeatBlock[]{
    const seatBlocks:SeatBlock[] = [];
    //check if section in the theatre
    mapInArray.forEach(section=>{
        let foundFreeSeats:Seat[] = [];
        //sort the seats one by one. example: 1 row 1 seat is better then 2 row 1 seat, 1 row 1 seat is better then 1 row 2 seat
        section.seats.sort((a: Seat, b: Seat) =>{
            return (a.row*1000 + a.column) - (b.row*1000 + b.column)
        }).forEach(seat=>{
            // if there is already a found free seat
            if(foundFreeSeats.length){
                //if the next seat is in a new row, we should save the found seats, but only if the seats amount is more then the asked seat number
                if(foundFreeSeats[0].row!== seat.row){
                    if(foundFreeSeats.length >= seatsNextToEachOther){
                        seatBlocks.push({
                            seats: foundFreeSeats.slice(),
                            totalPrice: 0
                        });
                     }
                     foundFreeSeats = []
                }else{
                    // if the two seats are in the same row and not reserved, we can save the seat. 
                    if(!seat.reserved){
                        foundFreeSeats.push(seat);
                    }else{
                        // if reserved, we should save the found seats, but only if the seats amount is more then the asked seat number
                        if(foundFreeSeats.length >= seatsNextToEachOther){
                            seatBlocks.push({
                                seats: foundFreeSeats.slice(),
                                totalPrice: 0
                            });
                        }
                        foundFreeSeats = []
                    }
                }
            }else if(!seat.reserved){
                 //not reserved, we can save the seat. 
                foundFreeSeats.push(seat);
            }
        })
    })
    return seatBlocks;
}

function createMap(){
    const auditorium = createAuditorium();
    const balconyMid = createBalconyMid();
    const balconyLeft = createSideBalcony(SectionType.BALCONY_LEFT);
    const balconyRight = createSideBalcony(SectionType.BALCONY_RIGHT);
    const boxLeftNo1 = createBox(SectionType.BOX_LEFT_1);
    const boxRightNo1 = createBox(SectionType.BOX_RIGHT_1);
    const boxLeftNo2 = createBox(SectionType.BOX_LEFT_2);
    const boxRightNo2 = createBox(SectionType.BOX_RIGHT_2);
    return {
        auditorium,
        balconyMid,
        balconyLeft,
        balconyRight,
        boxLeftNo1,
        boxRightNo1,
        boxLeftNo2,
        boxRightNo2,
    }
}

// create the box section by the given image
function createBox(sectionType: SectionType): Section {
    const section: Section = {
        type: sectionType,
        seats: [],
        priority: 4,
    }
    for(let i = 0; i<2 ; i++){
        const numberOfSeatsInRow = 3;
        for(let j = 0; j<numberOfSeatsInRow; j++){
            let seat = {
                row: i+1,
                column: j+1,
                price: -1,
                reserved: false,
                section
            }
            seat.price = i===0 ? 3000 : 2000;
            section.seats.push(seat)
        }
    }
    return section;

}

// create the side balcony section by the given image
function createSideBalcony(sectionType: SectionType): Section {
    const section: Section = {
        type: sectionType,
        seats: [],
        priority: 3
    }
    for(let i = 0; i<2 ; i++){
        const numberOfSeatsInRow = 4;
        for(let j = 0; j<numberOfSeatsInRow; j++){
            let seat = {
                row: i+1,
                column: j+1,
                price: -1,
                reserved: false,
                section
            }
            if(i===0){
                seat.price = j===3 ? 3000 : 4000;
            }else{
                seat.price = j===3 ? 2000 : 3000;
            }
            section.seats.push(seat)
        }
    }
    return section;

}

// create the mid balcony section by the given image
function createBalconyMid(): Section {
    const section: Section = {
        type: SectionType.BALCONY_MID,
        seats: [],
        priority: 2
    }
    for(let i = 0; i<2 ; i++){
        const numberOfSeatsInRow = i+18;
        for(let j = 0; j<numberOfSeatsInRow; j++){
            let seat = {
                row: i+1,
                column: j+1,
                price: -1,
                reserved: false,
                section
            }
            if(i===0){
                const numberOfYellowSeats = 4;
                if(j<(numberOfYellowSeats/2)){
                    seat.price = 4000;
                }else if(j<numberOfSeatsInRow-(numberOfYellowSeats/2)){
                    seat.price = 5000;
                }else{
                    seat.price = 4000;
                }
            }else{
                const numberOfGreenSeats = 4;
                const numberOfBlueSeats = 6;
                if(j<(numberOfGreenSeats/2)){
                    seat.price = 2000;
                }else if(j<(numberOfGreenSeats/2)+(numberOfBlueSeats/2)){
                    seat.price = 3000;
                }else if(j<numberOfSeatsInRow-((numberOfGreenSeats/2)+(numberOfBlueSeats/2))){
                    seat.price = 4000;
                }else if(j<(numberOfSeatsInRow)-(numberOfGreenSeats/2)){
                    seat.price = 3000;
                }else{
                    seat.price = 2000;
                }
            }
            section.seats.push(seat);
        }
    }
    return section;
}

// create the box Auditorium by the given image
function createAuditorium(): Section {
    const section: Section = {
        type: SectionType.AUDITORIUM,
        seats: [],
        priority: 1
    }

    for(let i = 0; i<8 ; i++){
        const numberOfSeatsInRow = i+14;
        for(let j = 0; j<numberOfSeatsInRow; j++){
            let seat = {
                row: i+1,
                column: j+1,
                price: -1,
                reserved: false,
                section
            }
            if(i<3){
                createFirstThreeRows(i, j, seat);
            }else if(i===3){
                createFourthRow(j, seat, numberOfSeatsInRow);
            }else if(i===4){
                const numberOfBlueSeats = 6
                createFifthRow(j, numberOfBlueSeats, seat);
            }else if(i===5){
                const numberOfBlueSeats = 10
                createSixthRow(j, numberOfBlueSeats, seat);
            }else{
                seat.price = 3000;
            }
            section.seats.push(seat);
        }
    }

    return section;
}



//helper functions

function createSixthRow(j: number, numberOfBlueSeats: number, seat: { row: number; column: number; price: number; }) {
    if (j < numberOfBlueSeats / 2) {
        seat.price = 3000;
    } else if (j < 14) {
        seat.price = 4000;
    } else {
        seat.price = 3000;
    }
}

function createFifthRow(j: number, numberOfBlueSeats: number, seat: { row: number; column: number; price: number; }) {
    if (j < numberOfBlueSeats / 2) {
        seat.price = 3000;
    } else if (j <= 14) {
        seat.price = 4000;
    } else {
        seat.price = 3000;
    }
}

function createFourthRow(j: number, seat: { row: number; column: number; price: number; }, numberOfSeatsInRow: number) {
    const numberOfYellowSeats = 4;
    const numberOfBlueSeats = 2;
    if (j < numberOfBlueSeats / 2) {
        seat.price = 3000;
    } else if (j < (numberOfYellowSeats + numberOfBlueSeats) / 2) {
        seat.price = 4000;
    } else if (j < 14) {
        seat.price = 5000;
    } else if (j < numberOfSeatsInRow - 1) {
        seat.price = 4000;
    } else {
        seat.price = 3000;
    }
}

function createFirstThreeRows(i: number, j: number, seat: { row: number; column: number; price: number; }) {
    const numberOfYellowSeats = i * 2;
    if (j < numberOfYellowSeats / 2) {
        seat.price = 4000;
    } else if (j < 14) {
        seat.price = 5000;
    } else {
        seat.price = 4000;
    }
}

