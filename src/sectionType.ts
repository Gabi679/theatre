export enum SectionType {
    BOX_LEFT_1,
    BOX_LEFT_2,
    BOX_RIGHT_1,
    BOX_RIGHT_2,
    BALCONY_RIGHT,
    BALCONY_LEFT,
    BALCONY_MID,
    AUDITORIUM
}