import { Seat } from "./seat";
import { SectionType } from "./sectionType";

export interface Section {
    type: SectionType;
    seats: Seat[];
    priority: number;
}